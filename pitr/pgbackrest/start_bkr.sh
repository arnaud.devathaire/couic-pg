#!/bin/bash

echo "Création du cluster PostgreSQL pgbackrest..."
pg_createcluster 16 pitr

echo "Mise à jour du postgresql.conf..."
CONFpg=$"/etc/postgresql/16/pitr/postgresql.conf"

echo "archive_mode = on" >> $CONFpg
echo "wal_level = replica" >> $CONFpg
echo "archive_command = 'pgbackrest --stanza=pitr archive-push %p'" >> $CONFpg
echo "archive_timeout = 180" >> $CONFpg

echo "Redémarrage de l'instance..."
pg_ctlcluster 16 pitr restart

echo "Mise à jour du pgbackrest.conf..."
su postgres << 'EOF'
chmod 777 /etc/pgbackrest.conf

CONFpgbackrest=$"/etc/pgbackrest.conf"
echo "[global]" > $CONFpgbackrest
echo "repo1-path=/tmp/pgbackrest" >> $CONFpgbackrest
echo "repo1-retention-full=1" >> $CONFpgbackrest
echo "repo1-retention-diff=2" >> $CONFpgbackrest
echo "" >> $CONFpgbackrest
echo "[pitr]" >> $CONFpgbackrest
echo "pg1-path=/var/lib/postgresql/16/pitr" >> $CONFpgbackrest
echo "pg1-host-user=postgres" >> $CONFpgbackrest
chmod 640 /etc/pgbackrest.conf

echo "Création du répertoire pgBackRest..."
mkdir /tmp/pgbackrest
chmod 700 -R /tmp/pgbackrest

echo "Création de la stanza..."
pgbackrest --stanza=pitr stanza-create
pgbackrest --stanza=pitr check

echo "Réalisation d'une sauvegarde complète..."
pgbackrest --stanza=pitr backup --type=full

echo "Informations sur la stanza :"
pgbackrest info
EOF

pg_lsclusters
