#!/bin/bash

echo "Création du cluster PostgreSQL rps..."
pg_createcluster 16 rps1

echo "Création du cluster PostgreSQL rps2..."
pg_createcluster 16 rps2

echo "Démarrage du service PostgreSQL rps1..."
pg_ctlcluster 16 rps1 start

echo "Création de la base de données 'replication'..."
su postgres << 'EOF'
PORT_rps1=$(grep "port = " /etc/postgresql/16/rps1/postgresql.conf | awk '{print $3}')
createdb -p $PORT_rps1 replication

echo "Préparation du secondaire 'rps2'..."
rm -rf /var/lib/postgresql/16/rps2/*

psql -p $PORT_rps1 -c "CREATE ROLE repuser WITH REPLICATION LOGIN PASSWORD 'repuser';"

export PGPASSWORD='repuser'

pg_basebackup -D /var/lib/postgresql/16/rps2/ -R -X s -c fast -C -P -S rps1 -h 127.0.0.1 -p $PORT_rps1 -U repuser
unset PGPASSWORD

pg_ctlcluster 16 rps2 start
pg_lsclusters
EOF
