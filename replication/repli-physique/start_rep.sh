#!/bin/bash

echo "Création du cluster PostgreSQL rep1..."
pg_createcluster 16 rep1

echo "Création du cluster PostgreSQL rep2..."
pg_createcluster 16 rep2

echo "Démarrage du service PostgreSQL rep1..."
pg_ctlcluster 16 rep1 start

echo "Création de la base de données 'replication'..."
su postgres << 'EOF'
PORT_rep1=$(grep "port = " /etc/postgresql/16/rep1/postgresql.conf | awk '{print $3}')
createdb -p $PORT_rep1 replication

echo "Préparation du secondaire 'rep2'..."
rm -rf /var/lib/postgresql/16/rep2/*

psql -p $PORT_rep1 -c "CREATE ROLE repuser WITH REPLICATION LOGIN PASSWORD 'repuser';"

export PGPASSWORD='repuser'

pg_basebackup -D /var/lib/postgresql/16/rep2/ -R -X s -c fast -P -h 127.0.0.1 -p $PORT_rep1 -U repuser
unset PGPASSWORD

pg_ctlcluster 16 rep2 start
pg_lsclusters
EOF
