#!/bin/bash

couic-pg_header() {
clear
echo "*****************************************************"
echo "+   ____ ___  _   _ ___ ____           ____   ____  +"
echo "+  / ___/ _ \| | | |_ _/ ___|         |  _ \ / ___| +"
echo "+ | |  | | | | | | || | |      _____  | |_) | |  _  +"
echo "+ | |__| |_| | |_| || | |___  |_____| |  __/| |_| | +"
echo "+  \____\___/ \___/|___\____|         |_|    \____| +"
echo "+                                                   +"
echo "*****************************************************"
echo ""
pg_lsclusters
echo ""
}
couic-pg_header
echo "Veuillez choisir une option :"
echo ""
echo "1) Replication"
echo "2) Sauvegarde"
echo ""
echo "3) Quitter"
echo ""
read -p "Entrez votre choix [1-3]: " choice

case $choice in
    1)
        couic-pg_header
        echo "1) rep1 + rep2 en réplication physique"
	    echo "2) rps1 + rps2 en réplication physique + slot"
        echo ""
        echo "3) Nettoyer les clusters rep[1-2]"
        echo "4) Nettoyer les clusters rps[1-2]"
        echo ""
        read -p "Entrez votre choix [1-5]: " subchoice
        case $subchoice in
            1)
                echo "Configuration de deux instances PostgreSQL en réplication physique..."
                sudo bash "./replication/repli-physique/start_rep.sh"
                ;;
            2)
                echo "Configuration de deux instances PostgreSQL en réplication physique avec slot..."
                sudo bash "./replication/repli-physique/start_rps.sh"
                ;;
	        3)
                echo "Exécution du script de nettoyage des clusters rep[1-2]..."
                sudo bash "./replication/repli-physique/stop_rep.sh"
                ;;
            4)
                echo "Exécution du script de nettoyage des clusters rps[1-2]..."
                sudo bash "./replication/repli-physique/stop_rps.sh"
                ;;
            *)
                echo "Choix invalide, veuillez réessayer."
                ;;
        esac
        ;;
    2)
        couic-pg_header
        echo "1) bkr + sauvegarde pgBackRest"
        echo "2) Nettoyer le cluster"
        echo ""
        read -p "Entrez votre choix [1-2]: " subchoice
        case $subchoice in
            1)
                echo "Exécution du script ..."
                sudo bash "./pitr/pgbackrest/start_bkr.sh"
                ;;
            2)
                echo "Exécution du script de nettoyage du cluster PostgreSQL..."
                sudo bash "./pitr/pgbackrest/stop_bkr.sh"
                ;;
            *)
                echo "Choix invalide, veuillez réessayer."
                ;;
        esac
        ;;
    3)
        echo "Quitter"
        exit 0
        ;;
    *)
        echo "Choix invalide, veuillez réessayer."
        ;;
esac
